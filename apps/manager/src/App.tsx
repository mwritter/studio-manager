import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Routes, Route, Link } from "react-router-dom";
import PresenterApp from "presenter/PresenterApp";
import SoundApp from "sound/SoundApp";
import Home from "./components/Home";

import "./index.css";

const App = () => (
  <div className="container">
    <div>Name: manager</div>
    <hr />
    <BrowserRouter>
      <nav>
        <Link to="/">Home | </Link>
        <Link to="/presenter">Presenter | </Link>
        <Link to="/sound">Sound</Link>
      </nav>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/presenter" element={<PresenterApp />} />
        <Route path="/sound" element={<SoundApp />} />
      </Routes>
    </BrowserRouter>
  </div>
);
ReactDOM.render(<App />, document.getElementById("app"));
