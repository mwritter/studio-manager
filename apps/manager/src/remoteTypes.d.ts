///<reference types="react" />
declare module "presenter/PresenterApp" {
  const PresenterApp: React.ComponentType;
  export default PresenterApp;
}

declare module "sound/SoundApp" {
  const SoundApp: React.ComponentType;
  export default SoundApp;
}
