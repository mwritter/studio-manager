import React from "react";
import ReactDOM from "react-dom";
import SoundApp from "./SoundApp";

import "./index.css";

const App = () => (
  <div className="container">
    <SoundApp />
  </div>
);
ReactDOM.render(<App />, document.getElementById("app"));
