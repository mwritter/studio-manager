import React from "react";
import ReactDOM from "react-dom";
import PresenterApp from './PresenterApp'
import "./index.css";

const App = () => (
  <div className="container">
    <PresenterApp />
  </div>
);
ReactDOM.render(<App />, document.getElementById("app"));
